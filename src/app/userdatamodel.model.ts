export class Userdatamodel {
    id: number;
    firstName: string;
    lastName: string;
    username: string;
    city: string;
    department: string;
}