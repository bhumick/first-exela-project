import { Injectable, Inject, OnInit } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Userdatamodel } from './userdatamodel.model';

@Injectable({
  providedIn: 'root'
})

export class LocalStorageService {    

  public currentData: any[] = [];

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) {} 

  saveInLocal(newUser:Userdatamodel){
    var y = this.storage.get('users');
    if(y != null || !y){
      this.currentData.push(newUser);
    }    
    this.storage.set('users',this.currentData);        
  }

  getFromLocal(){
    return this.storage.get('users');
  }

}
