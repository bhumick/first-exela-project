import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { LocalStorageService } from '../local-storage.service';
import { Userdatamodel } from '../userdatamodel.model';
import { MatSort, MatTableDataSource, MatPaginator, MatDialog, MatDialogConfig, MatSnackBar } from '@angular/material';
import { AddUserDialogComponent } from '../add-user-dialog/add-user-dialog.component';
import { EditUserDialogComponent } from '../edit-user-dialog/edit-user-dialog.component';
import { fromEvent, Observable } from 'rxjs';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
   
  addUserDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '400px';
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    let dialogRef = this.dialogBox.open(AddUserDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`);
      let localDataPostAdd = this.localStorageService.getFromLocal();
      this.localData(localDataPostAdd);
      this._snackBar.open(value,'Close', {
        duration: 3000
      }); 
    });
  }

  deleteEntry(user){
    //console.log(user);
    if(confirm("Are you sure you want to delete?")){
      let currentLocalData = this.localStorageService.getFromLocal();
      currentLocalData.forEach((val,ind) => {
        if(user.id === val.id){
          currentLocalData.splice(ind,1);
        }
      })
      // console.log('remains: '+ JSON.stringify(currentLocalData));
      currentLocalData.forEach((val,ind) => {
        this.localStorageService.saveInLocal(val);
      })
      this.localData(currentLocalData);
    }
  }

  editEntry(user){
    // console.log(JSON.stringify(user));
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = '400px';
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    let dialogRef = this.dialogBox.open(EditUserDialogComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(value => {
      //console.log(`Dialog sent: ${value}`);
      // let localDataPostAdd = this.localStorageService.getFromLocal();
      // this.localData(localDataPostAdd);
      this._snackBar.open(value,'Close', {
        duration: 3000
      }); 
    });
  }

  searchText:any;

  @ViewChild(MatSort,{static:true}) sort: MatSort; //select directive matSort
  
  @ViewChild(MatPaginator,{static:true}) paginator: MatPaginator; //select element mat-paginator
  length = 100;
  pageSize = 4;
  pageSizeOptions: number[] = [2, 4, 6, 8, 10];
  
  public dataSource = new MatTableDataSource<Userdatamodel>();

  //displayedColumns: string[] = ['firstname','lastname','username','city','department','edit','delete'];

  displayedColumns: string[] = ['firstname','lastname','username','city','department','delete'];

  constructor(private localStorageService: LocalStorageService, private dialogBox: MatDialog, private _snackBar: MatSnackBar) { }

  ngOnInit() {

    let x = this.localStorageService.getFromLocal();  
    this.dataSource.data = x as Userdatamodel[];
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  public localData(data:any){
    this.dataSource = data;
  }

  public search(val:any){
    this.dataSource.filter = val.trim().toLocaleLowerCase();
  }

  // addUserDialog(){
  //   const dialogRef = this.dialogBox.open(AddUserComponent,{
  //     width: '350px',
  //     height: '450px',
  //     disableClose: true, //to disable close outside box
  //     autoFocus: true //to focus on first input
  //     data:{
  //      message: "Hello Bhumik",
  //      buttonText:{
  //         cancel: 'Done'
  //       }
  //      }
  //   });
  // }


}
