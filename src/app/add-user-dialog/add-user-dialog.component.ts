import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LocalStorageService } from '../local-storage.service';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-add-user-dialog',
  templateUrl: './add-user-dialog.component.html',
  styleUrls: ['./add-user-dialog.component.scss']
})
export class AddUserDialogComponent implements OnInit {

  formGp: FormGroup;
  @Output() tableData = new EventEmitter();

  constructor(
    private matDialogRef: MatDialogRef<AddUserDialogComponent>,
    private fb: FormBuilder, 
    private localStorageService: LocalStorageService
    ) { }

    ngOnInit() {
      this.formGp = this.fb.group({
        // firstname: ['', Validators.required],
        // lastname: ['', Validators.required],
        id: [Math.floor(Math.random()*100)+1],
        firstname: [''],
        lastname: [''],
        username: [''],
        city: [''],
        department: [''],
      });
    }
  
    addUser(){
      this.localStorageService.saveInLocal(this.formGp.value);
      let localData = this.localStorageService.getFromLocal();
      this.tableData.emit(localData);
      this.formGp.reset();
      this.matDialogRef.close('Data Added Successfully!');      
    }

  close(){
    this.matDialogRef.close('Thank You!');
  }

}
