import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import { LocalStorageService } from '../local-storage.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {

  formGp: FormGroup;
  @Output() tableData = new EventEmitter(); 

  constructor(
    private fb: FormBuilder, 
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit() {
    this.formGp = this.fb.group({
      // firstname: ['', Validators.required],
      // lastname: ['', Validators.required],
      id: [Math.floor(Math.random()*100)+1],
      firstname: [''],
      lastname: [''],
      username: [''],
      city: [''],
      department: [''],
    });
  }

  addUser(){
    this.localStorageService.saveInLocal(this.formGp.value);
    let localData = this.localStorageService.getFromLocal();
    this.tableData.emit(localData);
    this.formGp.reset();
  }

}
